steal('jquery/class', 'jquery/model', 'jquery/dom/fixture', 'jquery/view/ejs', 'jquery/controller', 'jquery/controller/route', function( $ ) {

	/**
	 * @class Todo
	 * @parent index
	 * @constructor
	 * @author Wojciech Bednarski
	 * Creates a new todo.
	 */
	$.Model('Todo', {

		/**
		 * @function findAll
		 * Get all todos
		 * @return {Array} an array contains objects with all todos
		 */
		findAll: 'GET /todos',

		/**
		 * @function findOne
		 * Get todo by id
		 * @return {Object} an objects contains single todo
		 */
		findOne: 'GET /todos/{id}',

		/**
		 * @function create
		 * Create todo
		 * @param {Object} todo
		 * Todo object
		 * @codestart
		 * {name: 'read a book by Alfred Szklarski'}
		 * @codeend
		 *
		 * @return {Object} an object contains newly created todo
		 * @codestart
		 * {
		 *     id:   577,
		 *     name: 'read a book by Alfred Szklarski'
		 * }
		 * @codeend
		 *
		 * ### Example:
		 * @codestart
		 * var todo = new Todo({name: 'read a book by Alfred Szklarski'});
		 * todo.save(function (todo) {
		 *     console.log(todo);
		 * });
		 * @codeend
		 */
		create: 'POST /todos',

		/**
		 * @function update
		 * Update todo by id
		 * @return {Object} an object contains updated todo
		 */
		update: 'PUT /todos/{id}',

		/**
		 * @function destroy
		 * Destroy todo by id
		 * @return {Object} an object contains destroyed todo
		 */
		destroy: 'DELETE /todos/{id}'
	}, {

	});

	// Fixtures
	(function() {
		var TODOS = [
		// list of todos
		{
			id: 1,
			name: 'read The Good Parts'
		},
		{
			id: 2,
			name: 'read Pro Git'
		},
		{
			id: 3,
			name: 'read Programming Ruby'
		}];

		// findAll
		$.fixture('GET /todos', function() {
			return [TODOS];
		});

		// findOne
		$.fixture('GET /todos/{id}', function( orig ) {
			return TODOS[(+orig.data.id) - 1];
		});

		// create
		var id = 4;
		$.fixture('POST /todos', function() {
			return {
				id: (id++)
			};
		});

		// update
		$.fixture('PUT /todos/{id}', function() {
			return {};
		});

		// destroy
		$.fixture('DELETE /todos/{id}', function() {
			return {};
		});
	}());

	/**
	 * @class Todos
	 * Creates a new Todos controller
	 * @parent index
	 * @constructor
	 * @param {String} DOMElement DOM element
	 * @return {Object}
	 */
	$.Controller('Todos', {
		// init method is called when new instance is created
		'init': function( element, options ) {
			var findAll = Todo.findAll();
			this.element.html('resources/js/Todo/todos.ejs', findAll);
		},

		// add event listener to strong element on click
		'li strong click': function( el, e ) {
			// trigger custom event
			el.trigger('selected', el.closest('li').model());

			// log current model to the console
			console.log('li strong click', el.closest('.todo').model());
		},

		// add event listener to em element on click
		'li .destroy click': function( el, e ) {
			// call destroy on the model to prevent memory leaking
			el.closest('.todo').model().destroy();
		},

		// add event listener to Todo model on destroyed
		'{Todo} destroyed': function( Todo, e, destroyedTodo ) {
			// remove element from the DOM tree
			destroyedTodo.elements(this.element).remove();

			console.log('destroyed: ', destroyedTodo);
		}
	});

	/**
	 * @class Routing
	 * Creates application router
	 * @parent index
	 * @constructor
	 * @param {String} DOMElement DOM element
	 * @return {Object}
	 */
	$.Controller('Routing', {
		init: function() {
			new Todos('#todos');
		},

		// the index page
		'route': function() {
			console.log('default route');
		},

		// handle URL witch hash
		':id route': function( data ) {
			Todo.findOne(data, $.proxy(function( todo ) {
				// increase font size for current todo item
				todo.elements(this.element).animate({
					fontSize: '125%'
				}, 750);
			}, this));
		},

		// add event listener on selected
		'.todo selected': function( el, e, todo ) {
			// pass todo id as a parameter to the router
			$.route.attr('id', todo.id);
		}
	});

	// create new Routing controller instance
	new Routing(document.body);
});