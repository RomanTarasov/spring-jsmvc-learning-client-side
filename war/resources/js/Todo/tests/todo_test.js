steal('funcunit').then(function () {

        module('Todo app', {
            setup: function () {
                S.open('//todo/todo.html');
            }
        });

        test('page has #todos placeholder', function () {
            ok(S('body > #todos').size(), 'The #todo is child of the body');
        });
    }
);
