steal(
    'jquery/view/ejs',
    'jquery/class'
).then(
		function($){
			'use strict';
			
			$.Class('FirstClassTest',
			/* @static */
			{
			  count: 0
			},
			/* @prototype */
			{
			  init: function(args) {
				  console.log("class initiated");
				  var data = [];
				  $(args).replaceWith('resources/js/tti/views/templates/clientTasks.ejs', data);
				  //var template = $('resources/js/tti/views/templates/clientTasks.ejs', data);
			  },
			  slideDown: function(){}
			});
		}
);