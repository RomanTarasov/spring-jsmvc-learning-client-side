steal(
    'jquery/view/ejs',
    'jquery/class'
).then(
		function($){
			'use strict';
			
			$.Class('Dialog',
			/* @static */
			{
			
			},
			/* @prototype */
			{
			  init: function(args) {
				  this.render(args);
			  },
			  /**
			   * ejsFilePath
			   * dialogClassName
			   * buttonSubmitName
			   * model
			   * formData
			   * targetElement
			   */
			  render: function(_this){
				  _this.targetElement.append(_this.ejsFilePath, {}).find(_this.dialogClassName).dialog({
                    autoOpen: false,
                    modal:    true,
                    buttons:  [
                       	{
                    		text: _this.buttonSubmitName,
                            click: function () {
                                var self = this;
                                
                                $.each(_this.formData, function(name, value){
                                	if (!value){
                                		_this.formData[name] = $('input[name="'+name+'"]', this).val();
                                	}
                                });
                                
                                window.client = new _this.model(_this.formData);

                                window.client.save(function () {
                                    $(self).dialog('destroy').remove();
                                    _this.afterSubmit();
                                });

                            }
                    	},
                        {
                    		text: "Cancel",
                        	click: function () {
                                $(this).dialog('destroy').remove();
                            }
                        }
                    ],
                    close: function () {
                        $(this).dialog('destroy').remove();
                    }
                }).dialog('open');
			  }
			});
		}
);