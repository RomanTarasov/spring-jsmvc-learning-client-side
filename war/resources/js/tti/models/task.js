steal(
    'jquery/model',
    'vendors/pouchdb.js',
    'jquery/class',
    'jquery/view/ejs',
    'jquery/controller',
    'jquery/controller/route'
).then(
    function ($) {
        'use strict';

        // local variable to keep reference to time-tracker database
        var db;

        $.Model('TTI.Models.Task', {

                init: function () {
                    Pouch('idb://time-tracker', function (err, timeTracker) {
                        db = timeTracker;
                        $('body').trigger('TTI/db-ready');
                        console.log('TTI.Models.Task.init() | idb://time-tracker | err:', err, 'db:', db);
                    })
                },
                
                id:'id',
                
                create: function (task, success, error) {
                	$.ajax({
                		url: 'http://localhost:8080/helloproject/tasks',
                		type: 'post',
                		dataType: 'json',
                		contentType:"application/json",
                		data: JSON.stringify(task),
                		success: function (data, status, jqXHR) {
                            success(data);
                        },
                    
                        error: function (jqXHR, status) {            
                             error(status);
                        }
                		
                	});
                	
                },

                findOne: function (id, success, error) {
                	$.ajax({
                		url: 'http://localhost:8080/helloproject/tasks/' + id,
                		type: 'get',
                		dataType: 'json',
                		contentType:"application/json",
                		//data: JSON.stringify(task),
                		success: function (data, status, jqXHR) {
                            console.log('TTI.Models.Task.findOne() | GET | data:', data);
                            success(data);
                        },
                    
                        error: function (jqXHR, status) {            
                             error(status);
                        }
                		
                	});
                	
                },

                findAll: function (success, error) {
                	$.ajax({
                		url: 'http://localhost:8080/helloproject/tasks',
                		type: 'get',
                		dataType: 'json',
                		contentType:"application/json",
                		//data: JSON.stringify(task),
                		success: function (data, status, jqXHR) {
                            console.log('TTI.Models.Task.findAll() | GET | data:', data);
                            success(data);
                        },
                    
                        error: function (jqXHR, status) {            
                             error(status);
                        }
                		
                	});
                	
                },

                update: function (task, success, error) {
                    return db.put(task, function (err, response) {
                        console.log('TTI.Models.Task.update() | PUT | err:', err, 'client:', response);

                        if (response) {
                            success(response);
                        }
                        else if (err) {
                            error(err);
                        }
                    });
                },

                destroy: function (id, success, getError, removeError) {
                	$.ajax({
                		url: 'http://localhost:8080/helloproject/tasks/' + id,
                		type: 'delete',
                		dataType: 'json',
                		contentType:"application/json",
                		success: function (data, status, jqXHR) {
                            console.log('TTI.Models.Task.destroy() | DELETE | id:', id);
                            success(data);
                        },
                    
                        error: function (jqXHR, status) {            
                             alert(status);
                        }
                		
                	});
                	
                },
                getTasksByClientId: function(clientId, success, error){
                	$.ajax({
                		url: 'http://localhost:8080/helloproject/tasks/client/' + clientId,
                		type: 'post',
                		dataType: 'json',
                		contentType:"application/json",
                		success: function (data, status, jqXHR) {2
                            success(data);
                        },
                    
                        error: function (jqXHR, status) {            
                             alert(status);
                        }
                		
                	});
                }
            },
            {

            }
        );
    }
);
