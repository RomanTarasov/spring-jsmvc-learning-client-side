steal(
    'jquery/model',
    'jquery/dom/fixture',

    function ($) {
        'use strict';

        $.Model('TTI.Models.TaskNew', {
        	
        	id:'taskID',
        	
        	init: function () {
                // create database clients or get it if exists.
                console.log('TTI.Models.Client.init() | idb://clients | err:');

            },
            
            getTasksByClientId: function(id, success, error) {
            	$.ajax({
            		url: 'http://localhost:8080/helloproject/tasks/'+ id,
            		type: 'get',
            		dataType: 'json',
            		contentType:"application/json",
            		success: function (data, status, jqXHR) {
                        console.log('TTI.Models.Task.findAll() | GET | data:', data);
                        success(data);
                    },
                
                    error: function (jqXHR, status) {            
                         error(status);
                    }
            	});
            }
        },
            {

            }
        );
        
    	
        
    }
);
