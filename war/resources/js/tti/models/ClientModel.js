steal(
    'jquery/model',
    'jquery/dom/fixture',

    function ($) {
        'use strict';

        $.Model('TTI.Models.Client', {
        		
        		id:'id',
        	
        		
                init: function () {
                    // create database clients or get it if exists.


                    console.log('TTI.Models.Client.init() | idb://clients | err:');

                },

                findAll: function(success){
                	$.ajax({
                		url: 'http://localhost:8080/helloproject/clients',
                		type: 'get',
                		dataType: 'json',
                		contentType:"application/json",
                		//data: JSON.stringify(task),
                		success: function (data, status, jqXHR) {
                            console.log('TTI.Models.Task.findAll() | GET | data:', data);
                            success(data);
                        },
                    
                        error: function (jqXHR, status) {            
                             error(status);
                        }
                		
                	});
                },

                findOne: 'GET /clients/{id}',

                create: function (client, success, error) {
                	$.ajax({
                		url: 'http://localhost:8080/helloproject/clients',
                		type: 'post',
                		dataType: 'json',
                		contentType:"application/json",
                		data: JSON.stringify(client),
                		success: function (data, status, jqXHR) {
                            console.log('TTI.Models.Task.create() | POST | data:', data);
                            //var createdTask = JSON.parse(data);
                            success(data);
                			//createdTask = JSON.parse(data);
                        },
                    
                        error: function (jqXHR, status) {            
                             error(status);
                        }
                		
                	});
                },

                update: function (task, success, error) {
                	/*$.ajax({
                		url: 'http://localhost:8080/helloproject/client/' + id,
                		type: 'get',
                		dataType: 'json',
                		contentType:"application/json",
                		//data: JSON.stringify(task),
                		success: function (data, status, jqXHR) {
                            console.log('TTI.Models.Task.findOne() | GET | data:', data);
                            success(data);
                        },
                    
                        error: function (jqXHR, status) {            
                             error(status);
                        }
                		
                	});*/
                },

                destroy: function(id, success, getError, removeError){
                	$.ajax({
                		url: 'http://localhost:8080/helloproject/clients/' + id,
                		type: 'delete',
                		dataType: 'json',
                		contentType:"application/json",
                		success: function (data, status, jqXHR) {
                            console.log('TTI.Models.Client.destroy() | DELETE | id:', id);
                            success(data);
                        },
                    
                        error: function (jqXHR, status) {            
                             alert(status);
                        }
                		
                	});
                },
                
                getClientTasks: "GET /getClientTasks/{id}"
                	
            },
            {

            }
        );
        
    	
        
    }
);
