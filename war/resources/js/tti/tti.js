steal(
    function ($) {
        console.log('tti.js');
    },
    'vendors/jquery_ui/css/smoothness/jquery.ui.core.css',
    'vendors/jquery_ui/css/smoothness/jquery.ui.dialog.css',
    'vendors/jquery_ui/css/smoothness/jquery.ui.theme.css',
    'tti/views/styles/css/tti.css',
    'tti/views/styles/css/tti-client.css',
    'tti/models/task.js',
    'tti/models/ClientModel.js',
    'tti/controllers/tasks.js',
    'tti/controllers/router.js',
    'tti/controllers/navigation.js',
    'tti/controllers/ClientController.js',
    'steal/less'
).then(
    'vendors/jquery_ui/jquery.ui.core.js'
).then(
    'vendors/jquery_ui/jquery.effects.core.js'
).then(
    'vendors/jquery_ui/jquery.effects.highlight.js'
).then(
    'vendors/jquery_ui/jquery.ui.widget.js'
).then(
    'vendors/jquery_ui/jquery.ui.position.js',
    'vendors/jquery_ui/jquery.ui.dialog.js'
).then(
    function ($) {
        new TTI.Controllers.Navigation('#main-navigation ul');
    }
).then('./views/styles/css/tti.less');
