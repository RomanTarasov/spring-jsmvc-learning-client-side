module('tti/models/task', {
    setup: function () {
        console.log('setup');
    }
});

asyncTest('Create task', function () {
    console.log('Create task');
    expect(3);

    setTimeout(function () {
        var task = new TTI.Models.Task({
            name:        'The first task',
            hours:       341,
            cost:        {
                rate:  4,
                total: 10
            },
            description: ''
        });

        ok(task, 'new TTI.Models.Task - New instance.');

        task.save(function (data) {

            if (toString.call(data.id) === '[object String]') {
                ok(true, 'task.save - Returned object contains ID property which is String.');

                // clean up after create test
                TTI.Models.Task.destroy(data.id, function (data) {
                    ok(data, 'TTI.Models.Task.destroy - Destroyed object.');
                });
            }

        });

        start();

    }, 1000);

});

asyncTest('Create task with custom ID / Update task', function () {
    console.log('Create task with custom ID');
    expect(1);

    var task = {
        _id:         'custom-id',
        name:        'The first task',
        hours:       341,
        cost:        {
            rate:  4,
            total: 10
        },
        description: ''
    };

    TTI.Models.Task.update(task, function (data) {
        strictEqual(task._id, data.id, 'TTI.Models.Task.update - Updated successfully.');
    });

    start();

});

asyncTest('Find task', function () {
    console.log('Find task');
    expect(1);

    TTI.Models.Task.findOne('custom-id', function (data) {
        strictEqual('custom-id', data._id, 'TTI.Models.Task.findOne - Requested data has been received.');
    });

    start();

});

asyncTest('Find all tasks', function () {
    console.log('Find all tasks');
    expect(1);

    TTI.Models.Task.findAll(function (data) {
        ok(data, 'TTI.Models.Task.findAll - Returned object contains data.');
    });

    start();

});

asyncTest('Destroy task', function () {
    console.log('Destroy task');
    expect(1);

    TTI.Models.Task.destroy('custom-id', function (data) {
        ok(data, 'TTI.Models.Task.destroy - Destroyed object.');
    });

    start();

});
