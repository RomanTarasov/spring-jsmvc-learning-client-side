steal(
    'jquery/view/ejs',
    'jquery/controller',
    'jquery/dom/route'
).then(
    function ($) {
        'use strict';

        /**
         * @class TTI.Controllers.Navigation
         * Creates application main navigation controller
         * @parent index
         * @constructor
         * @param {String} DOMElement DOM element
         * @return {Object}
         */
        $.Controller('TTI.Controllers.Navigation', {
            init: function () {
                var navItems = [
                    {
                        name: 'Time Tracker',
                        className: 'time-tracker'
                    },
                    {
                        name: 'Invoice',
                        className: 'invoice'
                    },
                    {
                        name:      'Clients',
                        className: 'clients'
                    },
                    {
                        name: 'Reports',
                        className: 'reports'
                    },
                    {
                        name: 'Statistics',
                        className: 'statistics'
                    }
                    ];

                this.element.html('resources/js/tti/views/templates/navigation.ejs', navItems);
            },

            '.time-tracker click': function (e) {
                $.route.attr('page', 'time-tracker');
            },

            '.clients click': function (e) {
                $.route.attr('page', 'clients');
            }

        });

    }
);
