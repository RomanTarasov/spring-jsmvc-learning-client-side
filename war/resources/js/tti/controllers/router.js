steal(
    'tti/controllers/navigation.js',
    'tti/controllers/ClientController.js',
    'tti/controllers/tasks.js',
    'jquery/controller',
    'jquery/controller/route'
).then(
    function ($) {
        'use strict';

        /**
        * @class TTI.Controllers.Router
        * Creates application router
        * @parent index
        * @constructor
        * @param {String} DOMElement DOM element
        * @return {Object}
        */
        $.Controller('TTI.Controllers.Router', {
            init: function () {
                console.log('r init');
            },

            // the index page
            'route': function (e) {
                console.log('default route', e);
            },

            ':page route': function (data) {
                $('#content').empty().append('<div>');

                if (data.page === 'time-tracker') {
                    new TTI.Controllers.Tasks('#content div');
                }
                else if (data.page === 'clients') {
                    new TTI.Controllers.ClientController('#content div');
                }
            }

        });

        // create new Router controller instance
        $('body').bind('TTI/db-ready', function () {
            new TTI.Controllers.Router(document.body);
        });
    }
);
