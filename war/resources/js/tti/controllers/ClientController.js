steal(
    'jquery/view/ejs',
    'jquery/controller',
    '../widgets/tasks/tasks',
    'tti/models/ClientModel.js',
    'tti/models/TaskModel.js',
    'jquery/view/helpers',
    'tti/widgets/Dialog.js'
).then(
    function ($) {
        'use strict';

        console.log('TTI.Controllers.Client');

        /**
         * @class TTI.Controllers.Client
         * Creates a new Tasks controller
         * @parent index
         * @constructor
         * @param {String} DOMElement DOM element
         * @return {Object}
         */
        $.Controller('TTI.Controllers.ClientController', {
        	isTasksOpen: false,
            'init': function () {
                $('title').text('Clients | TTI');
                this.renderGrid();
            },
            
            renderGrid: function(){
            	var _this = this;
            	var data = TTI.Models.Client.findAll(function(data){
                	_this.element.html('resources/js/tti/views/templates/client/ClientsView.ejs', data);
                });
            },
        	
	        '{TTI.Models.Client} destroyed': function (Task, e, task) {
	            task.elements(this.element).remove();
	        },
	        
	        '.remove-client click': function (event) {
	        	if(!confirm("Are you sure you want to delete?")){
	        		return;
	        	}
	        	event.closest("tr").model().destroy();
	        },
	        
	        '.open-task click': function(event) {
	        	//TODO open close
	        	var _this = {};
	        	_this.event = event;
	        	_this.tasksDiv = $(event[0].link);
	        	if (this.isTasksOpen) {
	        		this.isTasksOpen = false;
	        		_this.tasksDiv.slideUp(function(){
	        			_this.tasksDiv.html('');
	        		});
	        		event.removeClass("open");
	        	} else {
	        		this.openClientTasks(event, _this);
	        	}
	        },
	        
	        openClientTasks: function(event, _this){
	        	this.isTasksOpen = true;
	        	
	        	try {
	        		_this.clientId = event.closest("tr").model().id;	        		
	        	} catch(err) {
	        		
	        	} finally {
	        		
	        	}
	        	
	        	/*TTI.Models.Task.getTasksByClientId(clientId, function(data){
	        		if (!data) { 
	        			data = {};
	        		};
	        		_this.tasksDiv.html('resources/js/tti/views/templates/client/TasksView.ejs', data);
	        		this.renderTasks(_this.tasksDiv, data);
	        	});*/
	        	this.renderTasks(_this);
	        	_this.tasksDiv.model(new TTI.Models.Task({clientId: _this.clientId, _this: _this}));
    			_this.tasksDiv.slideDown();
    			event.addClass("open");
	        },
	        
	        renderTasks: function(_this){
	        	TTI.Models.Task.getTasksByClientId(_this.clientId, function(data){
	        		if (!data) { 
	        			data = {};
	        		};
	        		_this.tasksDiv.html('resources/js/tti/views/templates/client/TasksView.ejs', data);
	        	});
	        },
	        
	        ".add-client click": function() {
	        	new Dialog({
     			   ejsFilePath: 'resources/js/tti/views/templates/client/AddNewClientDialog.ejs',
    			   dialogClassName: '.create-new-client',
    			   buttonSubmitName: 'Submit',
    			   model: TTI.Models.Client,
    			   formData: {
                       	name: null
                   },
                   targetElement: this.element
		    	});
	        },
	        
	        ".add-task click": function(event) {
	        	var _this = $(event).closest(".task-div-container").model()._this;
	        	_this.renderTasks = this.renderTasks;
	        	new Dialog({
     			   ejsFilePath: 'resources/js/tti/views/templates/client/AddNewTaskDialog.ejs',
    			   dialogClassName: '.create-new-task',
    			   buttonSubmitName: 'Submit',
    			   model: TTI.Models.Task,
    			   formData: {
                       	name: null,
                       	clientId: $(event).closest(".task-div-container").model().clientId
                   },
                   targetElement: this.element,
                   afterSubmit: function(){
                	   _this.renderTasks(_this);
                   }
		    	});
	        },
	        
	        '{TTI.Models.Client} created': function (Task, e, client) { 
	        	 this.renderGrid(); 
	        }

        });

    }
);
