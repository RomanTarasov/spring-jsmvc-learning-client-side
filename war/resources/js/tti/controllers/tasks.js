steal(
    'jquery/view/ejs',
    'jquery/view/helpers',
    'jquery/controller',
    'tti/models/task.js',
    'tti/models/ClientModel.js'
).then(
    function ($) {
        'use strict';

        console.log('TTI.Controllers.Tasks');

        /**
         * @class TTI.Controllers.Tasks
         * Creates a new Tasks controller
         * @parent index
         * @constructor
         * @param {String} DOMElement DOM element
         * @return {Object}
         */
        $.Controller('TTI.Controllers.Tasks', {
            'init': function (element, options) {
                var self = this;

                $('title').text('Time Tracker | TTI');
                
                TTI.Models.Task.findAll(function (data) {
                    if (!data.length) {
                        data = [
                            {
                                
                                    hours: '',
                                    cost: {
                                        total: ''
                                    },
                                    id: '',
                                    description: 'No tasks so far!',
                                    note: ''

                                
                            }
                        ];
                    }

                    self.element.html('resources/js/tti/views/templates/tasks/tasks.ejs', data);

                }, function(errorStat){
                	console.log("no tasks");
                	self.element.html('resources/js/tti/views/templates/tasks/tasks.ejs', {});
                });
                
                
                /*
                TTI.Models.Task.findAll(function (data) {
                    if (!data.rows.length) {
                        data.rows = [
                            {
                                doc: {
                                    hours: '',
                                    cost: {
                                        total: ''
                                    },
                                    id: '',
                                    description: 'No tasks so far!',
                                    note: ''

                                }
                            }
                        ];
                    }

                    self.element.html('resources/js/tti/views/templates/tasks/tasks.ejs', data.rows);

                });
                */

            },

            '{TTI.Models.Task} created': function (Task, e, task) {
                console.log('task', task);
                console.log('this.element', this.element);
                $('tbody tr:last', this.element).after('resources/js/tti/views/templates/tasks/task.ejs', task);
                $('tbody tr:last', this.element).effect('highlight', {}, 3000);
            },

            '{TTI.Models.Task} destroyed': function (Task, e, task) {
                task.elements(this.element).remove();
            },

            '.add-task click': function () {
            	var context = this;
            	var data = TTI.Models.Client.findAll(function(client) {context.element.append('resources/js/tti/views/templates/tasks/add_task.ejs', client).find('.create-new-task-dialog-form').dialog({
                    autoOpen: false,
                    modal:    true,
                    buttons:  {
                        'Create New Task': function () {
                            var self = this;
                            
                            window.task = new TTI.Models.Task({
                                hours: $('input[name="hours"]', this).val(),
                                id: $('input[name="task-id"]', this).val(),
                                cost: {
                                    rate: 0,
                                    total: 0
                                },
                                description: $('textarea[name="description"]', this).val(),
                                note: $('textarea[name="note"]', this).val()
                            });

                            window.task.save(function () {
                                $(self).dialog('destroy').remove();
                            });

                        },
                        Cancel: function () {
                            $(this).dialog('destroy').remove();
                        }
                    },
                    close: function () {
                        $(this).dialog('destroy').remove();
                    }                    
                }).dialog('open');
            	
            	$('.create-new-task-dialog-form #client_id').on('change', function(event){
            		console.log("client_id onchange fired");
            		
            		var client_id = $(this);
            		var task_id = $('#task_id');
            		task_id.empty();
            	    //$.each(result, function(item) {
            		task_id.append($("<option />").val("1").text('Task1'));
            		task_id.append($("<option />").val("2").text('Task2'));
            	    //});            		
            		
            		});
                });
            	
            	//var data = [{text: "xxx", value: "yyy"}];
                

            },
            
            '.delete-task click': function(event) {
            	console.log(event);
            	var confirmed = confirm("Are you sure you want to delete?");
            	
            	if(confirmed) {
            		//event.closest("tr").model().destroy();
            		$('.buttons').hide();
            	    $("table tr.task-selected").model().destroy();
            	}
            },
            
            'table tr.task click': function(event) {
            	
                //$('.buttons').css('top', event.offset().top);
                //$('.buttons').css('left',(event.offset().left + parseInt($(".tti_tasks").css('width'))));
                //$('.buttons').css('display','block');
            	
            	//$('.buttons').show();
            	
            	//$('.buttons').html(event.offset().top + " " + event.offset().left );
            	//$('.buttons').show();
            	
            	if($('.buttons').is(":visible")) {
            		//$.fx.off = true;
            		//$('.buttons').fadeTo(0,0);
            		$('.buttons').hide();
            		$('.task-selected').removeClass("task-selected");
              	} else {
              		//$.fx.off = false;
              	}
            	
            	$(".buttons").css({top: event.offset().top, 
            		               left: (event.offset().left + parseInt($(".tti_tasks").css('width'))), 
            		               position:'absolute'});
            	event.closest("tr").addClass("task-selected");
            	$('.buttons').show();
            	//$('.buttons').fadeOut(5000, function() {
            	//	$('.task-selected').removeClass("task-selected");
            	//});
            },
            
            /*
            'table mouseout': function(event) {
            	
            	$('.buttons').hide();
            }
            */
        });

    }
);
